/**
 * The list of Work Order Departments
 */
export type Departments =
  | "Appliance"
  | "Electrical"
  | "HVAC"
  | "Plumbing"
  | "Roofing";

/**
 * Each line item in a Work Order is defined
 * with the following characteristics. Thanks to
 * validated inputs, we know there should be no variance
 */
export interface WorkOrder {
  firstName: string;
  lastName: string;
  employeeNumber: number;
  department: Departments;
  part: number;
}

export type WorkOrdersByContactEmail = {
  [contactEmail: string]: WorkOrder[]
};

/**
 * Defines the array of valid keys to use for
 * the department lookup
 */
export type DepartmentLookup = {
  [Key in Departments]: string;
};
